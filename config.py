import os
basedir = os.path.abspath(os.path.dirname(__file__))

DEBUG_MODE = True
DB_NAME = os.path.join(basedir, 'app.db')
SQLALCHEMY_DATABASE_URI = 'sqlite:///' + DB_NAME
SQLALCHEMY_TRACK_MODIFICATIONS = False

TELEGRAM_BOT_TOKEN = 'YOUR_BOT_TOKEN_HERE'


def init_db():
    from watchbot import db
    from watchbot.models import Action
    db.session.add(Action(name="restart_miner", command="sudo reboot"))
    db.session.add(Action(name="shutdown_miner", command="sudo shutdown -h 0"))
    db.session.commit()
