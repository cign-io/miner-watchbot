# Installation

- export FLASK_APP=watchbot.py
- python setup.py install
- flask db upgrade
- flask shell
    - from config import init_db
    - init_db()
- flask run

## Donations

- BTC: 1MsefX6qpbQ3b1S1b6Xp5st2sUrpt6fYiD
- ETH: 0xfc21c6dfe099fa68cf11a53bf7e0502335556895
- VTC: Vs7SjL5ummc6GAf69cY69Q1x9odetJCZKd
- XMR: 432gjoY32Y2HzMCPDP16qYMGN1J4cQvEe13RsaewGFbtaiYLpPifJYiXcpkwkeC5RQ56aEid1yNUReyXQjvsFXaPP9eySxR
- AEON: WmtDEJLbEcXevqsuX8odfnNjxJFRYoyokDc13vdyYP7hcTCFcpMvKPNQzkZicD3K1REKoYBFF78nNKbnvnB5j3ie28M5DyutB
