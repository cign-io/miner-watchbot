from datetime import datetime

from flask import (
    jsonify,
    request,
)

from watchbot import (
    app,
    db,
)
from watchbot.models import (
    Action,
    MinerUpdates,
    RequestedAction,
)


@app.route('/')
def index():
    return "Use proper URL"


@app.route('/miner/<miner_id>/status', methods=['POST'])
def miner_status(miner_id):
    success = False
    try:
        db.session.add(
            MinerUpdates(
                miner_id=miner_id,
                raw_data=request.form['raw_data'],
            )
        )
        db.session.commit()
        success = True
    except Exception as exc:
        app.logger.exception(exc)

    return jsonify({
        'success': success,
    })


@app.route('/miner/<miner_id>/action', methods=['GET'])
def miner_action(miner_id):
    requested_action = RequestedAction.query.filter_by(
        miner_id=miner_id,
        processed=None,
    ).first()

    action = None
    if requested_action:
        requested_action .processed = datetime.utcnow()
        action = Action.query.get(requested_action.action_name)
        db.session.commit()

    return jsonify({
        'miner_id': miner_id,
        'action': action.command if action else '',
        'requested_on': requested_action.requested if requested_action else '',
    })
