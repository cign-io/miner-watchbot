BOOTING = 'booting'
UPDATING = 'updating'
UPDATED = 'updated'
ADL_ERROR = 'adl_error'
NO_CABLES = 'no_cables'
NO_MINE = 'no_mine'
NO_WATCHDOG = 'no_watchdog'
CONFIG_ERROR = 'config_error'
DISALLOWED = 'disallowed'
MINER_OFF = 'miner_off'
AUTOREBOOT = 'autoreboot'
DEFUNCT = 'defunct'
GPU_CRASHED = 'gpu_crashed'
OVERHEAT = 'overheat'
STARTED = 'started'
WORKING = 'working'

STATUS_MAP = {
    'starting ethos:': BOOTING,
    'do not reboot:': UPDATING,
    'reboot required:': UPDATED,
    'hardware error: possible': ADL_ERROR,
    'power cable problem:': NO_CABLES,
    'hardware error: graphics': NO_MINE,
    'no overheat protection:': NO_WATCHDOG,
    'config error:': CONFIG_ERROR,
    'miner disallowed:': DISALLOWED,
    'miner off:': MINER_OFF,
    'too many autoreboots:': AUTOREBOOT,
    'gpu crashed:': DEFUNCT,
    'gpu clock problem:': GPU_CRASHED,
    'overheat:': OVERHEAT,
    'miner started:': STARTED,
}

COMMAND_START = 'start'
COMMAND_MINER_STATUS = 'status'
COMMAND_RESET_MINER = 'reset'
COMMAND_SHUTDOWN_MINER = 'shutdown'
COMMAND_TOGGLE_UPDATES = 'updates'
COMMAND_ASSOCIATE_USER = 'associate'
COMMAND_CURRENT_CHAT_ID = 'chatid'
COMMAND_SET_DEFAULT_MINER = 'default'
COMMAND_EXTENDED_STATUS = 'extended'
COMMAND_FULL_STATUS = 'full'
COMMNAND_GET_HELP = 'help'
COMMAND_LIST_RIGS = 'list'

JOB_CHECK_MINER_STATUS = 'check_miner_status'
JOB_CHECK_MINER_INACTIVITY = 'check_miner_inactivity'

MSG_CANT_PERFORM_ACTION = "The user can't perform the action on this miner"
MSG_MISSING_MINER_ID = "Missing miner id, and no default miner has been set"
MSG_MISSING_ARGUMENTS = "There are one or more missing arguments"
MSG_NON_EXISTING_MINER = "That miner does not exist on our system"
MSG_NO_UPDATES = "There are no updates for that miner"
MSG_MINER_RESTART = "Miner {miner} commanded to restart"
MSG_MINER_SHUTDOWN = "Miner {miner} commanded to shutdown"
MSG_MAC_DOESNT_MATCH = "The provided mac does not match the miner mac"
MSG_MINER_NOT_ASSOCIATED = "That miner is already associated to an account"
MSG_CHAT_ID = "This is your chat id: {chat_id}. Don't lose it"
MSG_AUTO_UPDATE = "Updates for miner {miner_id} have been set to {status}"
MSG_MINER_STATUS_CHANGE = "Miner {miner}: Status changed from {old} to {new}"
MSG_MINER_BOOTED = "Miner {miner_id} just booted!"
MSG_NOT_EXISTING_COMMAND = "That's not an existing command"
MSG_MINERS_ON_ACCOUNT = """
These are the miners associated to your username: {miners}
"""
MSG_MINER_ASSOCIATED = """
Congratulations! The miner {miner} as been associated to your account
"""
MSG_MINER_SET_AS_DEFAULT = """
The miner {miner_id} has been set as the default for your account
"""
MSG_MINER_LAST_UPDATE = """
*Warning:* Last update from {miner_id} received 30 minutes ago
"""

MINER_CLIENT_DOWNLOAD_LINK = "http://bit.ly/minerwatch"

MINER_WELCOME_MESSAGE = """
Welcome {name}, to start monitoring your rigs, please download and
install the client for your miner from [this link]({download_link}).
Once installed, associate your user with that miner using the
command:
```  /associate [miner_id] [miner_mac]```

You can also get a more detailed information about how the commands
work by calling the */help* command.

"""

MINER_STATUS_MESSAGE = """
*Miner*: {miner_id}
*Status*: {miner_status}
*Hashrate*: {hashrate}
*Last update*: {last_update}

"""

MINER_EXTENDED_MESSAGE = """
*Miner*: {miner_id}
*Status*: {miner_status}
*Hashrate*: {hashrate}
*Uptime*: {time_up}
*Last update*: {last_update}
---
_Per card information_
*Hashes*: {gpu_hash}
*Fan speed*: {gpu_fan}
*Temps*: {gpu_temp}

"""

MINER_HELP_MESSAGE = """
To start using this bot, you need to download the client on your miner, and
set that script to auto execute on your *custom.sh* file. After that, your
rig should be ready to be associated to your account using the */associate*
command.

Once that is done, you can set that miner as your default one by using the
command */default [miner_id]*. This removes the necessity to specify a
miner on the following commands:

```
ø  updates
ø  status
ø  extended
ø  full
ø  reset
ø  shutdown
```

"""

# help - Show a more detailed help for the commands
# associate - Associate a miner to your account
# default - Set a miner as default
# updates - Toggle updates for a miner
# list - List all the rigs associated to your user
# status - Get status for a miner
# extended - Get extended status report for a miner
# full - Get full status report for a miner
# reset - Command a miner to restart
# shutdown - Command a miner to shutdown
