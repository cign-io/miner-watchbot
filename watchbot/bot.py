import json
import logging
from datetime import (
    datetime,
    timedelta,
)

from sqlalchemy.exc import IntegrityError
from telegram import ParseMode
from telegram.error import TelegramError

from telegram.ext import (
    CommandHandler,
    Filters,
    MessageHandler,
    Updater,
)

from config import TELEGRAM_BOT_TOKEN
from watchbot import (
    constants,
    db,
)
from watchbot.models import (
    MinerUpdates,
    RequestedAction,
    UserConfiguration,
)


class GenericMessageException(TelegramError):
    pass


class MinerDoesNotExistException(TelegramError):
    pass


class WrongArgumentException(TelegramError):
    pass


class WrongMinerIdException(TelegramError):
    pass


class UserNotAssociatedException(TelegramError):
    pass


class WatchBot(object):

    def __init__(self):
        """
        Configure the WatchBot with all the commands and job queues. To add a
        new command or job queue, just add it to the proper dictionary

        """
        self._updater = Updater(token=TELEGRAM_BOT_TOKEN)
        self._dispatcher = self._updater.dispatcher
        self._job_queue = self._updater.job_queue

        self._command_handlers = {
            constants.COMMAND_START: self._start,
            constants.COMMAND_MINER_STATUS: self._get_miner_status,
            constants.COMMAND_RESET_MINER: self._reset_miner,
            constants.COMMAND_SHUTDOWN_MINER: self._shutdown_miner,
            constants.COMMAND_TOGGLE_UPDATES: self._toggle_updates,
            constants.COMMAND_ASSOCIATE_USER: self._associate_user,
            constants.COMMAND_CURRENT_CHAT_ID: self._current_chat_id,
            constants.COMMAND_SET_DEFAULT_MINER: self._set_default_miner,
            constants.COMMAND_EXTENDED_STATUS: self._get_extended_status,
            constants.COMMNAND_GET_HELP: self._print_help_message,
            constants.COMMAND_LIST_RIGS: self._list_associated_rigs,
        }

        self._job_queues = {
            constants.JOB_CHECK_MINER_STATUS: (
                self._check_miner_status,
                60,
            ),
            constants.JOB_CHECK_MINER_INACTIVITY: (
                self._check_inactivity_up_to_thirty_minutes,
                60,
            ),
        }

    def __get_miner_id(self, username, params=None, strict_mode=False):
        """
        This function infers the proper miner_id to use, it is the one the
        user provided when calling the command, or if he set a default, that
        one, an exception is raised warning the user about trying to use a
        command that needs this parameter

        """
        if params:
            miner_id = params[0]
            is_default = False
        else:
            miner = UserConfiguration.query.filter_by(
                username=username,
                default_miner=True,
            ).first()
            miner_id = miner.miner_id if miner else None
            is_default = True

        if not miner_id:
            raise WrongMinerIdException('default')

        if strict_mode and not is_default:
            self.__validate_miner_permission(
                username=username,
                miner_id=miner_id,
            )

        return miner_id

    def __validate_miner_permission(self, username, miner_id):
        """
        Checks if a miner is associated to the user that requested an action

        """
        miner = UserConfiguration.query.filter_by(
            username=username,
            miner_id=miner_id,
        ).first()

        if not miner:
            raise UserNotAssociatedException(
                message=constants.MSG_CANT_PERFORM_ACTION,
            )

    def __error_handler(self, bot, update, error):
        """
        All Telegram errors will pass through this handler, if any particular
        error needs handling, here's the best place to do so. Any excetion
        that is not handled here will continue to raise to the general handler

        """
        try:
            raise error
        except WrongMinerIdException:
            text = constants.MSG_MISSING_MINER_ID
        except UserNotAssociatedException as exc:
            text = str(exc)
        except WrongArgumentException:
            text = constants.MSG_MISSING_ARGUMENTS
        except MinerDoesNotExistException:
            text = constants.MSG_NON_EXISTING_MINER
        except GenericMessageException as exc:
            text = str(exc)

        bot.send_message(
            chat_id=update.message.chat_id,
            text=text,
        )

    def _add_handlers(self):
        """
        This is responsible for associating all the handlers that are defined
        in _command_handlers with their respective functions, it also
        associates the UNKNOWN command, and the default error handler

        """
        for name, command in self._command_handlers.items():
            self._dispatcher.add_handler(
                CommandHandler(name, command, pass_args=True)
            )

        self._dispatcher.add_handler(
            MessageHandler(Filters.command, self._unknown)
        )
        self._dispatcher.add_error_handler(
            self.__error_handler,
        )

    def _add_job_queues(self):
        """
        This is responsible for associating all the job queues that are defined
        in _job_queues with their respective functions and times

        """
        for name, command in self._job_queues.items():
            self._job_queue.run_repeating(
                callback=command[0],
                interval=command[1],
                first=0,
            )

    def _start(self, bot, update, args):
        """
        Returns the welcome message for a new user, inviting him to download
        the miner client in charge of handling the rig, and also associate
        his miners with our bot

        """
        bot.send_message(
            chat_id=update.message.chat_id,
            text=constants.MINER_WELCOME_MESSAGE.format(
                name=update.message.chat.username,
                download_link=constants.MINER_CLIENT_DOWNLOAD_LINK,
            ),
            parse_mode=ParseMode.MARKDOWN,
        )

    def _print_help_message(self, bot, update, args):
        """
        Prints the help message back to the user

        """
        bot.send_message(
            chat_id=update.message.chat_id,
            text=constants.MINER_HELP_MESSAGE,
            parse_mode=ParseMode.MARKDOWN,
        )

    def _get_extended_status(self, bot, update, args):
        """
        If the user has set any miner as his default, that miner_id will be
        used if he didn't specify one when calling this function

        """
        miner_id = self.__get_miner_id(
            username=update.message.chat.username,
            params=args,
            strict_mode=False,
        )

        last_update = MinerUpdates.query.filter_by(
            miner_id=miner_id,
        ).order_by(MinerUpdates.id.desc()).first()

        if last_update:
            raw_data = json.loads(last_update.raw_data)
            bot.send_message(
                chat_id=update.message.chat_id,
                text=constants.MINER_EXTENDED_MESSAGE.format(
                    miner_id=last_update.miner_id,
                    miner_status=last_update.miner_status,
                    hashrate=last_update.hashrate,
                    time_up=str(timedelta(seconds=last_update.time_up)),
                    last_update=last_update.created.strftime("%x %X"),
                    gpu_hash=raw_data.get('miner_hashes'),
                    gpu_fan=raw_data.get('fanpercent'),
                    gpu_temp=raw_data.get('temp'),
                ),
                parse_mode=ParseMode.MARKDOWN,
            )
        else:
            bot.send_message(
                chat_id=update.message.chat_id,
                text=constants.MSG_NO_UPDATES,
            )

    def _get_miner_status(self, bot, update, args):
        """
        If the user has set any miner as his default, that miner_id will be
        used if he didn't specify one when calling this function

        """
        miner_id = self.__get_miner_id(
            username=update.message.chat.username,
            params=args,
            strict_mode=False,
        )

        last_update = MinerUpdates.query.filter_by(
            miner_id=miner_id,
        ).order_by(MinerUpdates.id.desc()).first()

        if last_update:
            bot.send_message(
                chat_id=update.message.chat_id,
                text=constants.MINER_STATUS_MESSAGE.format(
                    miner_id=last_update.miner_id,
                    miner_status=last_update.miner_status,
                    hashrate=last_update.hashrate,
                    last_update=last_update.created.strftime("%x %X"),
                ),
                parse_mode=ParseMode.MARKDOWN,
            )
        else:
            bot.send_message(
                chat_id=update.message.chat_id,
                text=constants.MSG_NO_UPDATES,
            )

    def _reset_miner(self, bot, update, args):
        """
        Sends a reset request to a specific miner, or the one set as the
        default for the user if no miner_id is provided

        """
        miner_id = self.__get_miner_id(
            username=update.message.chat.username,
            params=args,
            strict_mode=True,
        )

        db.session.add(RequestedAction(
            action_name='restart_miner',
            miner_id=miner_id,
        ))
        db.session.commit()
        bot.send_message(
            chat_id=update.message.chat_id,
            text=constants.MSG_MINER_RESTART.format(miner=miner_id),
        )

    def _shutdown_miner(self, bot, update, args):
        """
        Sends a shutdon request to a specific miner, or the one set as the
        default for the user if no miner_id is provided

        """
        miner_id = self.__get_miner_id(
            username=update.message.chat.username,
            params=args,
            strict_mode=True,
        )

        db.session.add(RequestedAction(
            action_name='shutdown_miner',
            miner_id=miner_id,
        ))
        db.session.commit()
        bot.send_message(
            chat_id=update.message.chat_id,
            text=constants.MSG_MINER_SHUTDOWN.format(miner=miner_id),
        )

    def _associate_user(self, bot, update, args):
        """
        Associates a Telegram user to an existing miner. To do so, we need
        to check if the user knows the miner id and its mac before accepting
        his request

        """
        miner_id = args[0]
        miner_mac = args[1]

        if not miner_id or not miner_mac:
            raise WrongArgumentException('default')

        last_update = MinerUpdates.query.filter_by(
            miner_id=miner_id,
        ).order_by(MinerUpdates.id.desc()).first()

        if not last_update:
            raise MinerDoesNotExistException('default')

        if last_update.mac_address != miner_mac:
            raise GenericMessageException(
                message=constants.MSG_MAC_DOESNT_MATCH,
            )

        db.session.add(UserConfiguration(
            miner_id=miner_id,
            username=update.message.chat.username,
            chat_id=update.message.chat_id,
        ))
        try:
            db.session.commit()
        except IntegrityError:
            raise UserNotAssociatedException(
                constants.MSG_MINER_NOT_ASSOCIATED,
            )

        bot.send_message(
            chat_id=update.message.chat_id,
            text=constants.MSG_MINER_ASSOCIATED.format(
                miner=miner_id,
            ),
        )

    def _current_chat_id(self, bot, update, args):
        """
        For testing or information purposes, this returns the chat id for
        the current user. It won't be made public to the user, but it's
        also not restricted

        """
        bot.send_message(
            chat_id=update.message.chat_id,
            text=constants.MSG_CHAT_ID.format(
                chat_id=update.message.chat_id,
            ),
        )

    def _toggle_updates(self, bot, update, args):
        """
        Toggles auto updates for the requested miner or the default one

        """
        miner_id = self.__get_miner_id(
            username=update.message.chat.username,
            params=args,
            strict_mode=True,
        )

        miner = UserConfiguration.query.get(miner_id)
        miner.auto_update = not miner.auto_update
        db.session.commit()

        bot.send_message(
            chat_id=update.message.chat_id,
            text=constants.MSG_AUTO_UPDATE.format(
                miner_id=miner_id,
                status='enabled' if miner.auto_update else 'disabled',
            ),
        )

    def _set_default_miner(self, bot, update, args):
        """
        Sets the received miner as the default, if it's associated to the user

        """
        username = update.message.chat.username
        miner_id = self.__get_miner_id(
            username=username,
            params=args,
            strict_mode=True,
        )

        db.session.execute(db.update(UserConfiguration).where(
            UserConfiguration.username == username,
        ).values(default_miner=False))

        miner = UserConfiguration.query.get(miner_id)
        miner.default_miner = True
        db.session.commit()

        bot.send_message(
            chat_id=update.message.chat_id,
            text=constants.MSG_MINER_SET_AS_DEFAULT.format(
                miner_id=miner_id,
            ),
        )

    def _list_associated_rigs(self, bot, update, args):
        """
        Returns the list of associated miners to the user's account

        """
        username = update.message.chat.username

        miners = UserConfiguration.query.filter_by(username=username).all()
        miner_list = ", ".join([x.miner_id for x in miners])

        bot.send_message(
            chat_id=update.message.chat_id,
            text=constants.MSG_MINERS_ON_ACCOUNT.format(
                miners=miner_list,
            ),
        )

    def _check_miner_status(self, bot, job):
        """
        Job queue that checks every miner with auto_update set to True. If its
        status is different than the one from a previous update, notify the
        user that has it associated to his account

        """
        rig_list = UserConfiguration.query.filter_by(auto_update=True).all()

        for miner in rig_list:
            last_updates = MinerUpdates.query.filter_by(
                miner_id=miner.miner_id
            ).order_by(MinerUpdates.created.desc()).limit(2).all()

            if last_updates[0].processed:
                # No updates for this miner
                continue

            new_status = last_updates[0].miner_status
            old_status = last_updates[1].miner_status

            if new_status in (
                constants.BOOTING,
                constants.STARTED,
            ):
                message = constants.MSG_MINER_BOOTED.format(
                    miner_id=miner.miner_id,
                )
            elif new_status != old_status:
                message = constants.MSG_MINER_STATUS_CHANGE.format(
                    miner=miner.miner_id,
                    old=old_status,
                    new=new_status,
                )
            else:
                # Same status as the old one, move on
                continue

            bot.send_message(
                chat_id=miner.chat_id,
                text=message,
            )

            last_updates[0].processed = datetime.utcnow()
            db.session.commit()

    def _check_inactivity_up_to_thirty_minutes(self, bot, job):
        """
        Job queue that will check if any miner rig has been inactive for over
        half an hour. Ideally, this will notify you only once, but it may do
        it twice at most

        """
        rig_list = UserConfiguration.query.filter_by(auto_update=True).all()

        for miner in rig_list:
            last_update = MinerUpdates.query.filter_by(
                miner_id=miner.miner_id
            ).order_by(MinerUpdates.created.desc()).first()

            past = datetime.utcnow() - timedelta(minutes=30)
            minute = timedelta(minutes=1)
            if (past - minute) <= last_update.created <= (past + minute):
                bot.send_message(
                    chat_id=miner.chat_id,
                    text=constants.MSG_MINER_LAST_UPDATE.format(
                        miner_id=miner.miner_id,
                    ),
                    parse_mode=ParseMode.MARKDOWN,
                )

    def _unknown(self, bot, update):
        """
        Prints 'Not an existing command' for every command we get that is not
        existing or hasn't been implemented yet

        """
        bot.send_message(
            chat_id=update.message.chat_id,
            text=constants.MSG_NOT_EXISTING_COMMAND,
        )

    def start_bot(self):
        """
        Sets up the process that connects with Telegram and associates all
        the commands and jobs for the queue

        """
        logging.basicConfig(
            format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
            level=logging.INFO
        )
        self._add_handlers()
        self._add_job_queues()
        self._updater.start_polling()
        self._updater.idle()
