import json
from datetime import datetime

from watchbot.constants import (
    STATUS_MAP,
    WORKING,
)
from watchbot import db


class Action(db.Model):
    __tablename__ = 'actions'

    name = db.Column(db.String(50), primary_key=True)
    command = db.Column(db.Text)

    def __init__(self, name, command):
        self.name = name
        self.command = command

    def __repr__(self):
        return '<Action {}>'.format(
            self.name,
        )


class RequestedAction(db.Model):
    __tablename__ = 'requested_actions'

    id = db.Column(db.Integer, primary_key=True)
    action_name = db.Column(db.String(50))
    miner_id = db.Column(db.String(20))
    requested = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)
    processed = db.Column(db.DateTime, nullable=True)
    last_update = db.Column(db.DateTime, onupdate=datetime.utcnow)

    def __init__(self, action_name, miner_id):
        self.action_name = action_name
        self.miner_id = miner_id

    def __repr__(self):
        return '<RequestedAction {}>'.format(
            self.miner_id,
        )


class MinerUpdates(db.Model):
    __tablename__ = 'miner_updates'

    id = db.Column(db.Integer, primary_key=True)
    miner_id = db.Column(db.String(20))
    miner_status = db.Column(db.String(20))
    hashrate = db.Column(db.String(20))
    gpus = db.Column(db.Integer)
    time_up = db.Column(db.Integer)
    mac_address = db.Column(db.String(20))
    raw_data = db.Column(db.Text)
    created = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)
    processed = db.Column(db.DateTime, nullable=True)
    last_update = db.Column(db.DateTime, onupdate=datetime.utcnow)

    def __init__(self, miner_id, raw_data):
        self.miner_status = WORKING
        dict_data = json.loads(raw_data)
        str_status = dict_data.get('status')
        status_candidate = [x for x in STATUS_MAP if str_status.startswith(x)]
        if status_candidate:
            self.miner_status = STATUS_MAP[status_candidate.pop()]
        self.raw_data = raw_data
        self.miner_id = miner_id
        self.hashrate = dict_data.get('hash')
        self.gpus = dict_data.get('gpus')
        self.mac_address = dict_data.get('mac')
        self.time_up = dict_data.get('uptime')

    def __repr__(self):
        return '<MinerUpdates {}>'.format(
            self.miner_id,
        )


class UserConfiguration(db.Model):
    __tablename__ = 'user_configurations'

    miner_id = db.Column(db.String(20), primary_key=True)
    username = db.Column(db.String(50))
    chat_id = db.Column(db.Integer())
    auto_update = db.Column(db.Boolean, default=True)
    default_miner = db.Column(db.Boolean, default=False)
    created = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)
    last_update = db.Column(db.DateTime, onupdate=datetime.utcnow)

    def __init__(self, miner_id, username, chat_id):
        self.miner_id = miner_id
        self.username = username
        self.chat_id = chat_id

    def __repr__(self):
        return '<UserConfiguration {}>'.format(
            self.miner_id,
        )
