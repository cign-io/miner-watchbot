import json
from multiprocessing import Process
from subprocess import (
    call,
    check_output,
)
from time import sleep
from urllib import urlencode
from urllib2 import urlopen

###############################################################################
#                               GETTING STARTED                               #
###############################################################################

# Assuming you downloaded this file into your home directory, to configure this
# client, you only need to add the following line to your custom.sh file:
# python /home/ethos/rig_watcher.py &

# Also, if you do not know how to get your miner id and/or mac, you can run the
# script like this:
# python rig_watcher.py

# The first line will tell you how to link your miner with your telegram user,
# once you get that first line, press Ctrl + C to end the process

# Change this to True if you want to prevent the command job to handle your rig
READ_ONLY_MODE = False

###############################################################################
# DO NOT MODIFY ANYTHING BEYOND THESE LINES UNLESS YOU KNOW WHAT YOU ARE DOING
###############################################################################

# Don't modify these constants
SERVER_URL = 'https://rigs.cign.io'
UPDATE_JOB_TIME = 300
COMMAND_JOB_TIME = 60
MINER_ID = ''
MINER_MAC = ''
STATS_COMMAND = 'require("/opt/ethos/lib/functions.php"); echo json_encode(get_stats());'  # noqa


def send_miner_status():
    try:
        print('Started process to send miner status')
        while True:
            result = check_output(
                args='php -r \'{}\''.format(STATS_COMMAND),
                shell=True,
            )
            print('Sending miner status')
            send_request_to_server(
                path='/miner/{}/status'.format(MINER_ID),
                params={
                    'raw_data': result,
                },
            )
            sleep(UPDATE_JOB_TIME)
    except KeyboardInterrupt:
        pass
    except Exception as exc:
        print('Something bad happened: {}'.format(exc))


def check_for_commands():
    if READ_ONLY_MODE:
        print('Process running in read only mode')
        return

    try:
        print('Started process to check for miner jobs')
        while True:
            sleep(COMMAND_JOB_TIME)
            print('Searching for commands to run')
            response = send_request_to_server(
                path='/miner/{}/action'.format(MINER_ID)
            )
            if (
                response['action'] and
                response['miner_id'] == MINER_ID
            ):
                print('Running `{}`'.format(response['action']))
                call(response['action'], shell=True)
    except KeyboardInterrupt:
        pass
    except Exception as exc:
        print('Something bad happened: {}'.format(exc))


def send_request_to_server(path, params=None):
    encoded_data = urlencode(params) if params else None
    response = urlopen(
        SERVER_URL+path,
        encoded_data,
    ).read()
    return json.loads(response)


def startup_config():
    global MINER_ID
    global MINER_MAC
    result = check_output('php -r \'{}\''.format(STATS_COMMAND), shell=True)
    parsed = json.loads(result)
    MINER_ID = parsed['hostname']
    MINER_MAC = parsed['mac']

    if not (MINER_ID and MINER_MAC):
        print('Client not properly configured, exiting')
        exit()
    else:
        print('To associate this miner, execute this command on your phone:')
        print('/associate {miner} {mac}'.format(
            miner=MINER_ID,
            mac=MINER_MAC,
        ))


if __name__ == '__main__':
    startup_config()

    updates = Process(target=send_miner_status)
    commands = Process(target=check_for_commands)

    updates.start()
    commands.start()

    try:
        updates.join()
        commands.join()
    except KeyboardInterrupt:
        print('Exiting')
