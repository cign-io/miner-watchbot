# Installing the client on your local PC

* Download the file to your home directory (1)
* Run it once to get your miner id and mac (2)
* Add it to your custom.sh file (3)

## Commands

1. **Download:** `curl -L http://bit.ly/minerwatch -o rig_watcher.py`
2. **Run:** `python rig_watcher.py`
3. **Custom.sh:** `python /home/ethos/rig_watcher.py &`

## How it works

The client sends the same information that is sent to your personal
ethosdistro address to our servers, then, whenever you want to know
your rig status, you just need to run any of the informative commands
that exits.

You can also command your rig to restart or shutdown using the proper
commands. The client will check every minute if there's any new command
to be processed and executed

## Existing commands

- *help* - Shows a more detailed help for the commands
- *associate* - Use it to associate a rig to your Telegram user
- *default* - Set a miner as your default for some commands
- *updates* - Toggle updates for a miner you have associated
- *list* - List all the rigs associated to your user
- *status* - Get status for a specific or your default miner
- *extended* - Get extended status report for a specific or default miner
- *full* - Get full status report for a specific or default miner
- *reset* - Command a specific or default miner to restart
- *shutdown* - Command a specific or default miner to shutdown

## Donations

- BTC: 1MsefX6qpbQ3b1S1b6Xp5st2sUrpt6fYiD
- ETH: 0xfc21c6dfe099fa68cf11a53bf7e0502335556895
- VTC: Vs7SjL5ummc6GAf69cY69Q1x9odetJCZKd
- XMR: 432gjoY32Y2HzMCPDP16qYMGN1J4cQvEe13RsaewGFbtaiYLpPifJYiXcpkwkeC5RQ56aEid1yNUReyXQjvsFXaPP9eySxR
- AEON: WmtDEJLbEcXevqsuX8odfnNjxJFRYoyokDc13vdyYP7hcTCFcpMvKPNQzkZicD3K1REKoYBFF78nNKbnvnB5j3ie28M5DyutB
